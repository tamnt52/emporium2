require 'test_helper'

class Admin::AuthorTest < ActiveSupport::TestCase
  #fixtures :authors
  test "test_name" do
    author = Admin::Author.create(:first_name => 'Joel',:last_name => 'Spolsky')
    assert_equal 'Joel', author.first_name
    assert_equal 'Joel Spolsky', author.name
  end
  # test "the truth" do
  #   assert true
  # end
end
