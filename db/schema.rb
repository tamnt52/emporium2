# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160605071654) do

  create_table "admin_authors", force: :cascade do |t|
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "authors_books", id: false, force: :cascade do |t|
    t.integer "author_id", limit: 4, null: false
    t.integer "book_id",   limit: 4, null: false
  end

  add_index "authors_books", ["author_id"], name: "fk_bk_authors", using: :btree
  add_index "authors_books", ["book_id"], name: "fk_bk_books", using: :btree

  create_table "books", force: :cascade do |t|
    t.string   "title",        limit: 255,   default: "str"
    t.integer  "publisher_id", limit: 4,                     null: false
    t.datetime "published_at"
    t.string   "isbn",         limit: 13
    t.text     "blurb",        limit: 65535
    t.integer  "page_count",   limit: 4
    t.float    "price",        limit: 24
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "books", ["publisher_id"], name: "fk_books_publishers", using: :btree

  create_table "publishers", force: :cascade do |t|
    t.string   "publisher",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "authors_books", "admin_authors", column: "author_id", name: "fk_bk_authors", on_delete: :cascade
  add_foreign_key "authors_books", "books", name: "fk_bk_books", on_delete: :cascade
  add_foreign_key "books", "publishers", name: "fk_books_publishers", on_delete: :cascade
end
