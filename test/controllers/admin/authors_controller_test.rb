require 'test_helper'

class Admin::AuthorsControllerTest < ActionController::TestCase
  setup do
    @admin_author = admin_authors(:one)
  end

  test "test_show" do
    get :show, :id => 1
    assert_template 'admin/authors/show'
    assert_equal 'Joel', assigns(:author).first_name
    assert_equal 'Spolsky', assigns(:author).last_name
  end

  test "test_index" do
    get :index
    assert_response :success


  end
  test "test_new " do
    get :new
    assert_template 'admin/authors/new'
    assert_select 'h1', :content => 'Create new author'
    assert_select 'form', :attributes => {:action => '/admin/authors/create'}
  end

  test "test_failing_create" do
    assert_no_difference('Admin::Author.count') do
      post :create, :admin_author => {:first_name => 'Joel',:last_name => 'Spolsky'}
      assert_response :success
      assert_template 'admin/authors/new'
      assert_select :tag => 'div', :attributes => {:class => 'fieldWithErrors'}
    end
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_authors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_author" do
    assert_difference('Admin::Author.count') do
      post :create, admin_author: { first_name: @admin_author.first_name, last_name: @admin_author.last_name }
    end

    assert_redirected_to admin_author_path(assigns(:admin_author))
  end

  test "should show admin_author" do
    get :show, id: @admin_author
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_author
    assert_response :success
  end

  test "should update admin_author" do
    patch :update, id: @admin_author, admin_author: { first_name: @admin_author.first_name, last_name: @admin_author.last_name }
    assert_redirected_to admin_author_path(assigns(:admin_author))
  end

  test "should destroy admin_author" do
    assert_difference('Admin::Author.count', -1) do
      delete :destroy, id: @admin_author
    end

    assert_redirected_to admin_authors_path
  end
end
