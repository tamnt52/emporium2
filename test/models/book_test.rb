require 'test_helper'

class BookTest < ActiveSupport::TestCase
  fixtures  :publishers, :books, :authors
  test "test_create" do
    apress = Publisher.find_by_publisher("Apress")
    num_apress = apress.books.size

    book = Book.new(
        :title => 'Rails E-Commerce 3nd Edition',
        :authors =>( Admin::Author.find(1)),
        :published_at => Time.now,
        :isbn => '123-123-123-x',
        :blurb => 'E-Commerce on Rails',
        :page_count => 300,
        :price => 30.5
    )
    apress.books << book
    apress.reload
    book.reload
    assert_equal num_apress+1, apress.books.size
    assert_equal 'Apress', book.publisher.publisher
  end

end
